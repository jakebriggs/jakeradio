#!/bin/bash 

#. settings.sh
. "$1"

# echo $tempprefix
# echo "${tempprefix}song"
# echo $databasefile
# echo $collections
# make the database if it doesn't exist
if [ ! -e "$databasefile" ]
then
  cat /dev/null > "$databasefile"
  /usr/bin/sqlite3 "$databasefile" "create table songs (id INTEGER PRIMARY KEY,filepath TEXT)"
  /usr/bin/sqlite3 "$databasefile" "create table played (id INTEGER PRIMARY KEY, songid INTEGER, playedtimestamp DATETIME DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY(songid) REFERENCES songs(id))"
fi

numsongs=`cat "${tempprefix}song" | wc -l`

# test to see if it should be a info track or a normal track
if [ "$numsongs" -gt "$maxtracks" ]
then
  old_IFS=$IFS
  IFS=$'\n'

  filecontent=( `cat "${tempprefix}song" ` )

  for aline in "${filecontent[@]}"
  do
    thesong=`/usr/bin/getsonginfo.sh "$aline"`
    songlist="$songlist. $thesong"
  done

  IFS=$old_IFS
  songlist=`echo "$songlist" | cut -c 36-`
  songlist="The previous songs were $songlist"
  
  #/usr/bin/pico2wave -l en-GB -w "${tempprefix}out.wav" "$songlist" >/dev/null 2>&1
  # echo "$songlist"  
  echo "$songlist" | /usr/bin/piper/piper --model /usr/bin/piper/en_GB-alba-medium.onnx --output_file "${tempprefix}out.wav" >/dev/null 2>&1

  /usr/bin/sox -v 1.1 "${tempprefix}out.wav" -c 2 -r 44100  "${tempprefix}outstereo.wav" >/dev/null 2>&1
  /usr/bin/lame --tt "Recent Tracks" --ta "Jakeradio Robot Announcer"  -b 128 -m j -S "${tempprefix}outstereo.wav" "${tempprefix}out.mp3" >/dev/null 2>&1
	
  afile="${tempprefix}out.mp3"
  # blank the file
  echo "" > "${tempprefix}song"
else
  found="0"
  
  #echo " "
  #echo "========================="
  #echo "Looking for a new track"
  
  while [ $found -eq 0 ]
  do
    #afile=`find /data/music/ /data/Carlys\ Music/ | grep -i .mp3 | shuf -n 1`
	
    collectionindex=`expr $RANDOM % ${#collections[@]}`
    whichcollection=${collections[$collectionindex]}

    # echo "collectionindex $collectionindex"
    # echo "whichcollection $whichcollection"
    # echo find "$whichcollection" -maxdepth 1 | shuf -n 1 --random-source /dev/urandom
    
    anartist=`find "$whichcollection" -maxdepth 1 | shuf -n 1 --random-source /dev/urandom`
    afile=`find "$anartist" -type f | grep -i .mp3 | shuf -n 1 --random-source /dev/urandom`

    # echo "checking '$whichcollection' '$anartist' '$afile'"
    if  [[ !  -z  $afile  ]]
    then
      echo "Checking $afile" >> /tmp/songlog	    
      len=`mp3info -p "%S" "$afile"`
      
      shortenough=0

      lenmessage="too long $len"
      if [ $len -lt $maxlenseconds ]
      then
        shortenough=1
	lenmessage="short enough $len"
      fi
      echo "$lenmessage" >> /tmp/songlog

      afileescaped=`echo "$afile" | sed "s/'/''/g"`
      playedrecently=`/usr/bin/sqlite3 $databasefile "select count(songs.filepath) as playcount from played inner join songs on played.songid = songs.id where playedtimestamp > datetime('now','-1 day') and songs.filepath like '$afileescaped'"`

      notplayedrecenntly=0
      playedmessage="played too recently $playedrecently -gt $maxdailyplays"
      if [ $playedrecently -lt $maxdailyplays ]
      then
        notplayedrecenntly=1
	playedmessage="not played too recently $playedrecently -lt $maxdailyplays"
      fi
      echo "$playedmessage"  >> /tmp/songlog
      
      foundmessage="not found a good mp3"
      if [ $shortenough -eq 1 ] && [ $notplayedrecenntly -eq 1 ]; then
        found=1
	foundmessage="found a good mp3"
      fi
      echo "$foundmessage short enough $shortenough and not played recently $notplayedrecenntly"  >> /tmp/songlog

    fi
  done

  echo $afile >> ${tempprefix}song 
fi

# put the track in the db, or update the playcount if its already there
afileescaped=`echo "$afile" | sed "s/'/''/g"`

songid=`sqlite3 $databasefile "SELECT id FROM songs WHERE '$afileescaped' =  filepath"`

if [ "$songid" == "" ]
then
  sqlite3 $databasefile "insert into songs (filepath) values ('$afileescaped')"
  songid=`sqlite3 $databasefile "SELECT id FROM songs WHERE '$afileescaped' =  filepath"`
fi
  
sqlite3 $databasefile "insert into played (songid) values ($songid)"

echo $afile

