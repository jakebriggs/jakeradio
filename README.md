# jakeradio
Depends on liquidsoap, sqlite, pico2wave, sox, and lame

## THE BASH SCRIPT
- usr/bin/getrandomsong.sh


The only argument is the path to a file with some settings. First it decides if the maxtracks threshold has been breached, if so, it produces an "info" mp3 that describes the last maxtracks in a robot voice. 
Otherwise, it chooses a random track. First it chooses a random "collection" - a root directory where music is. Then, it chooses a random directory in that collection, then it chooses a random file in that directory. 

Basically I have my music like `/data/jakesmusic/<artist>/<album>/<song>` and `/data/someone-elses-music/<artist>/<album>/<song>` and I want equal chance for each collection to be picked from, and equal chance for each artist to be picked from. If its pure random, then artists with more songs get played more.
The song is put into a sqlite database and returned to liqiudsoap.


## LIQIUDSOAP

- etc/liquidsoap/jake.liq
- etc/liquidsoap/jakeradio.sh

Each .liq file is another source for icecast, so for multiple streams use multiple .liq files pointing to different icecast mount points
I use liquidsoap to call the bash script to get the path, then play it through an icecast mount.
I use Apache to reverse proxy to icecast, so I can get at the stream over port 80.


The `etc/liquidsoap/jakeradio.sh` is the settings file, it defines the name of the radio, the database file, the max tracks to play in a row before the robot dj announces whats played, the longest song it'll pick (I think this is broken though) and the maximum number of times to play a song a day. It also defines where you may want a temp directory and stuff since it store transiant info there.


```
--------  8<  --------
radioname="jake"
databasefile="/home/liquidsoap/${radioname}radio.db"

maxtracks=4
# 15 minutes
maxlenseconds=900
maxdailyplays=1

tempdir='/tmp'
tempprefix="$tempdir/$radioname"

#collections=("/data/music" "/data/Carlys Music")
collections=("/data/music")
--------  8<  --------
```


For the playlist, I use a bash script to get the playlist data and send it in json with some angular to a web browser, the web browser makes the table. usage like http://foo.bar.com/cgi-bin/playlistangular?jake to view the tracklist in 
`/home/liquidsoap/jakeradio.db`
